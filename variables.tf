# Common configurations
variable "tags" {
  description = "A map of string containing key value pair to tag created resources."
  type        = map(string)
  default     = {}
}

variable "s3_buckent_name" {
  description = "The S3 bucket name that will be created and for which we will create the write only profile"
  type        = string
  default     = "doctolib"
}
variable "allowed_object_prefix" {
  description = "The S3 prefix object for which we will create the write only profile"
  type        = string
  default     = "can_be_written"
}