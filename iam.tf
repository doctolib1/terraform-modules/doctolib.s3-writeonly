resource "aws_iam_role" "s3_writeonly_role" {
  name = "s3_writeonly_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = var.tags
}

resource "aws_iam_instance_profile" "s3_writeonly_role_profile" {
  name = "s3_writeonly_role_profile"
  role = aws_iam_role.s3_writeonly_role.name
}

resource "aws_iam_role_policy" "s3_writeonly_role_policy" {
  name = "s3_writeonly_role_policy"
  role = aws_iam_role.s3_writeonly_role.id

  policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect":"Allow",
      "Action":["s3:ListBucket", "s3:ListAllMyBuckets", "s3:GetBucketLocation"],
      "Resource":"arn:aws:s3:::*"
    },
    {
      "Effect":"Deny",
      "Action":["s3:ListBucket"],
      "NotResource":["${aws_s3_bucket.write_only_bucket.arn}","${aws_s3_bucket.write_only_bucket.arn}/*"]
    },
    {
      "Effect":"Allow",
      "Action":["s3:ListBucket","s3:PutObject"],
      "Resource":["${aws_s3_bucket.write_only_bucket.arn}/${var.allowed_object_prefix}/*"]
    }
  ]
}
EOF
}