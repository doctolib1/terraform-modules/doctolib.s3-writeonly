output "s3_writeonly_role_profile_name" {
  description = "The AWS s3 write only role profile name"
  value       = aws_iam_instance_profile.s3_writeonly_role_profile.name
}