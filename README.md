# doctolib.s3-writeonly

This Terraform module aims to provide an easy way to deploy an S3 bucket and create an associate profile allowing to write object from a specified prefix on AWS.
This module provision : 

- A S3 bucket
- An IAM role, policy and profile

## Requirements

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.15 |
| aws | >= 3.39.0|

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.39.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| tags | A map of string containing key value pair to tag created resources. | `map(string)` | n/a | yes |
| s3\_buckent\_name | The list of authorized IPs. | `string` | `doctolib` | no |
| allowed\_object\_prefix | The S3 prefix object for which we will create the write only profile. | `string` | `can_be_written`| no |

## Outputs

| Name | Description |
|------|-------------|
| s3\_writeonly\_role\_profile\_name | The AWS s3 write only role profile name |