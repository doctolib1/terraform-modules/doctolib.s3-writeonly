resource "aws_s3_bucket" "write_only_bucket" {
  bucket = var.s3_buckent_name
  acl    = "private"

  tags = var.tags
}